(function () {
'use strict';

var
	inputs = [
		{	array: [],
			name: '[], should not iterate'
		},
		{	array: [1, 2, 3],
			name: '[1, 2, 3], should iterate 3 times'
		},
		{	array: { length: 3 },
			name: 'array-like object, length == 3, should iterate 3 times'
		},
		{	array: { length: 3.5 },
			name: 'non-integer length property, ' +
				'should consider integer part of length, like [].slice'
		}
	],
	fails = [
		{	array: null,
			name: 'arr is null or undefined, ' +
				'should not iterate and handle exception in callback'
		},
		{	array: false,
			name: 'NaN length property, ' +
				'should not iterate and handle exception in callback'
		},
		{	array: { length: -3 },
			name: 'negative length property, ' +
				'should not iterate and handle exception in callback'
		}
	],
	fn = Function.prototype;

QUnit.module('smooth');

inputs.forEach(function (el) {
	QUnit.test(
		el.name,
		function (assert) {
			var
				length = Math.floor(el.array.length),
				output = [],
				tail = assert.async();

			asyncForeach(
				el.array,
				function log(item, index, done) {
					setTimeout(function () {
						output.push(String(item));
						done();
					});
				},
				function end(err) {
					assert.equal(
						output.length,
						length,
						'length: ' + length
					);
					assert.ok(
						output.every(function (a, i) {
							return a === String(el.array[i]);
						}),
						'output: ' + output.join(', ')
					);
					assert.equal(
						err,
						undefined,
						'error: ' + String(err)
					);
					tail();
				}
			);
		}
	);
});

QUnit.test(
	'array which length exceeds max stack size, ' +
		'should not overflow stack, forcing async iteration',
	function (assert) {
		var
			input = new Array(6000)
				.join('1')
				.split('1')
				.map(function (el, i) { return i + 1; }),
			output = [],
			length = input.length,
			tail = assert.async();

		asyncForeach(
			input,
			function log(item, index, done) {
				output.push(item);
				done();
			},
			function end(err) {
				assert.deepEqual(
					output,
					input,
					'output is array of integers from 1 to 6000'
				);
				assert.equal(
					err,
					undefined,
					'error: ' + String(err)
				);
				tail();
			}
		);
	}
);

QUnit.test('[1, 2, 3] with descending timeouts, should preserve order',
	function (assert) {
		var
			input = [1, 2, 3],
			output = [],
			tail = assert.async();

		asyncForeach(
			input,
			function log(item, index, done) {
				setTimeout(function () {
					output.push(item);
					done();
				}, 300 - index * 100);
			},
			function end(err) {
				assert.deepEqual(
					output,
					input,
					'output: ' + output.join(', ')
				);
				assert.equal(
					err,
					undefined,
					'error: ' + String(err)
				);
				tail();
			}
		);
	}
);

QUnit.module('fails');

fails.forEach(function (el) {
	QUnit.test(
		el.name,
		function (assert) {
			var
				output = [],
				tail = assert.async();

			asyncForeach(
				el.array,
				function log(item, index, done) {
					setTimeout(function () {
						output.push(String(item));
						done();
					});
				},
				function end(err) {
					console.log(err);
					assert.deepEqual(
						output,
						[],
						'0 iterations until error'
					);
					assert.ok(
						err instanceof Error,
						'error: ' + String(err)
					);
					tail();
				}
			);
		}
	);
});

QUnit.test(
	'iterator is not a function, ' +
		'should not iterate and handle exception in callback',
	function (assert) {
		var
			input = [1, 2, 3],
			output = [],
			tail = assert.async();

		assert.expect(2);
		asyncForeach(
			input,
			null,
			function end(err) {
				console.log(err);
				assert.deepEqual(
					output,
					[],
					'0 iterations until error'
				);
				assert.ok(
					err instanceof Error,
					'error: ' + String(err)
				);
				tail();
				// if end throws, we dont handle
			}
		);
	}
);

QUnit.test(
	'iterator throws later in the process, ' +
		'should iterate until error and handle exception in callback',
	function (assert) {
		var
			input = [fn, fn, null, fn],
			output = [],
			tail = assert.async();

		assert.expect(4);
		asyncForeach(
			input,
			function callItem(item, index, done) {
				item();
				output.push(item);
				assert.ok(true, 'iteration ' + (index + 1));
				done();
			},
			function end(err) {
				console.log(err);
				assert.deepEqual(
					output,
					[fn, fn],
					'2 iterations until error'
				);
				assert.ok(
					err instanceof Error,
					'error: ' + String(err)
				);
				tail();
				// if end throws, we dont handle
			}
		);
	}
);

QUnit.test('invalid callback, should throw',
	function (assert) {
		function doneNotFunction() {
			asyncForeach([1, 2, 3], fn, 1);
		}

		assert.throws(
			doneNotFunction,
			new TypeError('done is not a function'),
			'throws: done is not a function'
		);
	}
);
})();
