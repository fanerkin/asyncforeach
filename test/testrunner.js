var QUnit = require('qunitjs');
var qunitTap = require('qunit-tap');

global.asyncForeach = require('../');
qunitTap(QUnit, console.log);

require('../test/asyncForeach-test.js');

QUnit.load();
