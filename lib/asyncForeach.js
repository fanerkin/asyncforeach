function asyncForeach(arr, iterator, done) {
	var
		length,
		later =
			typeof setImmediate == 'function' ?
				setImmediate :
				setTimeout;

	function tryAction(i) {
		try {
			iterator(
				arr[i],
				i,
				iterate.bind(null, ++i)
			);
		} catch (ex) {
			return done(ex);
		}
	}

	function iterate(i) {
		if (i >= length) return later(done);

		later(tryAction.bind(null, i));
	}

	if (typeof done != 'function') {
		throw new TypeError('done is not a function');
	}

	if (
		!arr ||
		isNaN(arr.length) ||
		arr.length < 0
	) {
		return later(done.bind(
			null, new TypeError('arr is not valid')
		));
	}

	length = Math.floor(arr.length);
	iterate(0);
}

if (module && module.exports) module.exports = asyncForeach;
