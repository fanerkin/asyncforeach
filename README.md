# asyncForeach

### features
* serial: one by one iteration
* permissive: only requires valid callback, provides any error to it
* async: one iteration in a macrotask

### usage
* available globally in browser
```
<script src="path/to/asyncForeach.js"></script>
```
* or require in CommonJS
```
require('path/to/asyncForeach')
```

### tests
* in terminal (faster, minimal report)
```
npm test
```
* or open test.html in browser (slower, more informative)
